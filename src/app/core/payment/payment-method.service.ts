import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import {HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class PaymentMethodService {
  
  constructor(private http:HttpClient) { }

  generateToken():Observable<any>{
    return this.http.get<any>(environment.test_OM +"/token")
  }

  generateUrlPayment(dat:any):Observable<any>{
    
    var headers_object = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':"Bearer "+ dat.token_om,
      'Accept':'application/json',
      'Cache-Control': 'no-cache'

    });
    console.log(dat.token_om)
    const httpOptions = {

      headers: headers_object,
    };
    console.log(httpOptions)

    

    return this.http.post<any>(environment.test_OM+"/url",{amount:dat.amount,order_id:dat.order_id},httpOptions);
   
    
  }
// les parametres sont a revoir 
  transac_Status(pay_token:any,amount:any,token:any,ref:any):Observable<any>{
    var headers_object = new HttpHeaders({
    'Content-Type':'application/json',
    'Authorization':"Bearer "+ token,
    'Accept':'application/json'
    

  });
  console.log(token)
  const httpOptions = {

    headers: headers_object,
  };
  console.log(httpOptions)

  

  return this.http.post<any>(environment.test_OM+"/transaction/status",{amount:amount,order_id:ref,pay_token:pay_token},httpOptions);
 
  
}

  
}
